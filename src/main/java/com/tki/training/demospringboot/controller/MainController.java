/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tki.training.demospringboot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author yudi
 */
@RestController
public class MainController {
    
    @GetMapping("/")
    public String home() {
        return "Hello World";
    }
    
    @GetMapping("/{message}")
    public String showMessage(@PathVariable String message) {
        return message;
    }
    
}
