/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tki.training.demospringboot.controller;

import com.tki.training.demospringboot.dao.StudentDao;
import com.tki.training.demospringboot.entity.Student;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author yudi
 */
@RestController
@RequestMapping("/student")
public class StudentController {
    
    @Autowired
    private StudentDao studentDao;
    
    @GetMapping()
    public List<Student> findAll() {
        List<Student> listStudent = studentDao.findAll();
        
        return listStudent;
    }
    
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    public Student insert(@RequestBody Student student) {
        //insert
        studentDao.save(student);
        
        if (student.getHeight() == 100) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Buat gagal manual.");
        }
        
        //update
        student.setHeight(0);
        studentDao.save(student);
        
        return student;
    }
    
    @PutMapping("/{id}")
    public Student update(@RequestBody Student student, @PathVariable long id) {
        Optional<Student> optionalStudent = studentDao.findById(id);
        if (optionalStudent.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data tidak ditemukan. Id : " + id);
        }
        student.setId(id);
        studentDao.save(student);
        
        return student;
    }
    
    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        studentDao.deleteById(id);
    }
}
