/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tki.training.demospringboot.controller;

import com.tki.training.demospringboot.dao.StudentDao;
import com.tki.training.demospringboot.dto.StudentDto;
import com.tki.training.demospringboot.entity.Student;
import com.tki.training.demospringboot.util.StudentUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author yudi
 */
@RestController
@RequestMapping("/test")
public class TestController {
    
    @Value( "${server.name}" )
    private String serverName;
    
    @Autowired
    private StudentDao studentDao;
    
    @Autowired
    private StudentUtil studentUtil;
    
    @GetMapping("/{testMessage}")
    public String testShowMessage(@PathVariable String testMessage) {
        return testMessage;
    }
    
    @GetMapping("/properties")
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String getPropertiesValue() {
        return serverName;
    }
    
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Student testPost(@RequestBody Student student) {
        boolean isExist = studentUtil.checkDoubleData();
        
        if (isExist) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Data sudah ada");
        }
        
        studentDao.save(student);
        
        return student;
    }
    
    @PostMapping("/student")
    public StudentDto testPostStudent(@RequestBody StudentDto student) {
        return student;
    }
    
    @PutMapping()
    public String testPut() {
        return "OK";
    }
    
    @DeleteMapping()
    public String testDelete() {
        return "OK";
    }
}
