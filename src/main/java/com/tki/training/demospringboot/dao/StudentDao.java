/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tki.training.demospringboot.dao;

import com.tki.training.demospringboot.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author yudi
 */
@Service
public interface StudentDao extends JpaRepository<Student, Long> {
    
}
