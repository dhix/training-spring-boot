/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tki.training.demospringboot.schedule;

import java.util.Date;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author yudi
 */
@Component
public class ScheduleTask {
    
    @Scheduled(fixedDelay = 5000)
    public void taskPrintOut() {
        System.out.println("Date time : " + new Date());
    }
    
    @Scheduled(cron = "0 1 0 * * *") // every day 00:01:00
    public void taskPrintOut2() {
        System.out.println("Date time : " + new Date());
    }
    
    @Scheduled(cron = "0 1 0 1 * *") // every month 00:01:00
    public void taskPrintOut3() {
        System.out.println("Date time : " + new Date());
    }
}
